﻿using System;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;

namespace Maximin
{
    public partial class MainForm : Form
    {
        private bool _isExecuting;
        private Maximin _maximin;
        private bool _isMaximin = true;
        private Bitmap _image;
        private Kmeans _kmeans;

        public MainForm()
        {
            InitializeComponent();
            
            toolTip.SetToolTip(PointsTrackBar, PointsTrackBar.Value.ToString());
        }

        private void trackBar_Scroll(object sender, EventArgs e)
        {
            toolTip.SetToolTip((Control)sender, ((TrackBar) sender).Value.ToString());
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            _isExecuting = !_isExecuting;
            if (_isExecuting)
            {
                PointsTrackBar.Enabled = false;
                _isMaximin = true;
                executeButton.Text = @"Остановить";
                _maximin = new Maximin(PointsTrackBar.Value);
                timer.Start();
            }
            else
            {
                PointsTrackBar.Enabled = true;
                executeButton.Text = @"Кластеризировать";
                timer.Stop();
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!_isExecuting) 
                return;

            if (_isMaximin)
            {
                _image = _maximin.Cluster();
                if (_image != null)
                {
                    pictureBox.Image = _image;
                }
                else
                {
                    _isMaximin = false;
                    _kmeans = new Kmeans(PointsTrackBar.Value, _maximin.GetCenters());
                }
            }
            else
            {
                pictureBox.Image = _kmeans.Cluster();
            }
        }
    }
}