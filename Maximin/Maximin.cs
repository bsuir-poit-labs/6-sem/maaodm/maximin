﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using static System.Drawing.Color;
using static System.Math;

namespace Maximin
{
    public class Maximin
    {
        private readonly int _width;
        private readonly int _height;
        private int _classesCount;

        private List<Point> _centers;

        private readonly int[,] _pointsClasses;

        private readonly List<double> _destToCenters;

        private readonly Color[] _colors =
        {
            Red, Blue, Green, Purple, Brown,
            Yellow, Wheat, Cyan, Lime, Pink,
            Gray, SkyBlue, Maroon, Orange, Fuchsia,
            Indigo, Teal, Goldenrod, Olive, DarkGray
        };

        private Point[] _oldCenters;

        public Maximin(int pointsCount)
        {
            _height = _width = (int) Ceiling(Sqrt(pointsCount));
            _pointsClasses = new int[_width, _height];

            _destToCenters = new List<double>();

            InitialCenters();
        }

        private void InitialCenters()
        {
            var random = new Random();
            _centers = new List<Point>
            {
                new(random.Next(_width), random.Next(_height))
            };

            var secondCenter = GetFurthestPoint(_centers[0]);
            _centers.Add(secondCenter);
            _classesCount = 2;
        }

        private Point GetFurthestPoint(Point center)
        {
            Point furthestPoint = default;
            double maxDistance = 0;
            for (int y = 0; y < _height; y++)
            {
                for (int x = 0; x < _width; x++)
                {
                    var distance = CalcDistance(center, x, y);
                    if (distance > maxDistance)
                    {
                        furthestPoint.X = x;
                        furthestPoint.Y = y;
                        maxDistance = distance;
                    }
                }
            }

            return furthestPoint;
        }

        public Bitmap Cluster()
        {
            ClassifyPoints();
            var bitmap = DrawBitmap();
            ReCalcCenters();

            return _oldCenters.SequenceEqual(_centers) ? null : bitmap;
        }

        private void ClassifyPoints()
        {
            for (int y = 0; y < _height; y++)
            {
                for (int x = 0; x < _width; x++)
                {
                    _pointsClasses[x, y] = GetClosestCenterIndex(x, y);
                }
            }
        }

        private int GetClosestCenterIndex(int x, int y)
        {
            _destToCenters.Clear();
            for (int i = 0; i < _classesCount; i++)
            {
                _destToCenters.Add(CalcDistance(_centers[i], x, y));
            }

            var minDestination = _destToCenters[0];
            var minIndex = 0;
            for (var i = 1; i < _classesCount; i++)
            {
                if (_destToCenters[i] < minDestination)
                {
                    minDestination = _destToCenters[i];
                    minIndex = i;
                }
            }

            return minIndex;
        }

        private void ReCalcCenters()
        {
            _oldCenters = _centers.ToArray();
            var threshold = GetTreshold();

            var maxDistances = new double[_classesCount];
            var furthestPoints = new Point[_classesCount];

            for (int y = 0; y < _height; y++)
            {
                for (int x = 0; x < _width; x++)
                {
                    var distance = CalcDistance(_centers[_pointsClasses[x, y]], x, y);
                    if (distance > maxDistances[_pointsClasses[x, y]])
                    {
                        maxDistances[_pointsClasses[x, y]] = distance;
                        furthestPoints[_pointsClasses[x, y]].X = x;
                        furthestPoints[_pointsClasses[x, y]].Y = y;
                    }
                }
            }

            for (int i = 0; i < maxDistances.Length; i++)
            {
                if (maxDistances[i] > threshold)
                {
                    _centers.Add(furthestPoints[i]);
                    _classesCount++;
                }
            }
        }

        private double GetTreshold()
        {
            double sumDist = 0.0;
            int distCount = 0;

            for (int i = 0; i < _classesCount; i++)
            {
                for (int j = i + 1; j < _classesCount; j++)
                {
                    sumDist += CalcDistance(_centers[i], _centers[j]);
                    distCount++;
                }
            }

            return sumDist / distCount / 2;
        }

        private double CalcDistance(Point p1, Point p2)
        {
            return Sqrt(Pow(p1.X - p2.X, 2) + Pow(p1.Y - p2.Y, 2));
        }

        private double CalcDistance(Point point, int x, int y)
        {
            return Sqrt(Pow(point.X - x, 2) + Pow(point.Y - y, 2));
        }

        private Bitmap DrawBitmap()
        {
            var bitmap = new Bitmap(_width, _height);
            for (int y = 0; y < _width; y++)
            {
                for (int x = 0; x < _height; x++)
                {
                    bitmap.SetPixel(x, y, _colors[_pointsClasses[x, y] % _colors.Length]);
                }
            }

            foreach (var center in _centers)
            {
                bitmap.SetPixel(center.X, center.Y, Black);
            }

            return bitmap;
        }

        public Point[] GetCenters()
        {
            return _centers.ToArray();
        }
    }
}